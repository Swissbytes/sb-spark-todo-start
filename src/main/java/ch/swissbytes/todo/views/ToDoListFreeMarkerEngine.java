package ch.swissbytes.todo.views;

import freemarker.template.Configuration;
import spark.template.freemarker.FreeMarkerEngine;

public class ToDoListFreeMarkerEngine extends FreeMarkerEngine {
    public ToDoListFreeMarkerEngine() {
        setConfiguration(createDefaultConfiguration());
    }

    private Configuration createDefaultConfiguration() {
        Configuration configuration = new Configuration();
        configuration.setClassForTemplateLoading(ToDoListFreeMarkerEngine.class, "");
        return configuration;
    }
}
